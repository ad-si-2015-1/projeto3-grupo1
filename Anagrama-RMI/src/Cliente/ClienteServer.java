package Cliente;


import java.rmi.Naming;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matheus
 */
public class ClienteServer {

    public ClienteServer() {
        try{
            Cliente c = new ClienteImpl();
            Naming.rebind("rmi://localhost:1099/ClienteService", c);
        }
        catch(Exception e){
            
        }
        
    }
    
    public static void main(String[] args) {
        new ClienteServer();
    }
    
}
