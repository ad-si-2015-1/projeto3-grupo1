package Cliente;


import java.rmi.Remote;
import java.rmi.RemoteException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matheus
 */
public interface Cliente extends Remote{
    public abstract void novoAnagrama() throws RemoteException;
    public abstract void desclassificar() throws RemoteException;
    public abstract void vencer() throws RemoteException;

}
